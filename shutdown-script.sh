ls | grep .yaml | while read -r line; do
  echo "Processing $line"
  kubectl delete -f $line
done
