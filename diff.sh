#!/bin/bash

ls | grep .yaml | while IFS= read -r line; do
  echo "Processing $line"
  if [[ "$line" == "secret.yaml" ]]; then
    continue
  fi
  kubectl diff -f $line
done
